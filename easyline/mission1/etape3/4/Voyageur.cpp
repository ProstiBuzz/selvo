#include <iostream>
#include <string>
#include "Voyageur.h"
using namespace std;

Voyageur::Voyageur(string nom, int age)
{
    //On définie la taille du nom minimum du voyageur
    while (nom.length() < 2)
    {
        cout << "Quel est le nom du voyageur? " << endl;
        getline(cin,nom);
    }
    this->nom=nom;
    
    //On définie l'âge minimum du voyageur
    while (age < 0)
    {
        cout << "Quel est l'age du voyageur? "<< endl;
        cin >> age;
    }
    this->age=age;

    this->setCategorie();
}
Voyageur::Voyageur(){}

//On appelle notre fonction d'affichage
void Voyageur::afficheDonnees()
{
    cout << "Voyageur: "<< this->nom <<endl;
    cout << "Age: " << this->age << endl;
    cout << "Categorie: " << this->categorie << endl;
};

//On vérifie la taille du nom
void Voyageur::setNom(string nom){
    while(nom.length() < 2)
    {
        cout << "Quel est le nom du voyageur? " << endl;
        getline(cin,nom);
    }
    this->nom;
}
string Voyageur::getNom(){
    return this->nom;
}


//On vérifie l'âge 
void Voyageur::setAge(int age){
    while(age <0)
    {
        cout << "Quel est l'age du voyageur? " << endl;
        cin >> age;
    }
    this->age;
    this->setCategorie();
}
int Voyageur::getAge(){
    return this->age;
}

void Voyageur::setCategorie(){
   
    if (this->age < 1)
    {
        this->categorie = "Nourrisson";
    }

    else if (this->age < 18)
    {
        this->categorie = "Enfant";
    }
        
    else if (this->age <60)
    {
        this->categorie = "Adulte";
    }

    else
    {
        this->categorie = "Senior";
     }
}
string Voyageur::getCategorie(){
    return this->categorie;
}
int main()
{
    Voyageur voyageur1;
    voyageur1.setNom("jo");
    voyageur1.setAge(18);

    //Initialisation d'objet avec paramètres par l'utilisateur
    string nom;
    int age;

    cout << "Quel est le nom du voyageur? " << endl;
    getline(cin,nom);

    cout << "Quel est l'age du voyageur? " << endl;
    cin >> age;


    Voyageur voyageur2(nom,age);
    voyageur2.afficheDonnees();

    return 0;
}