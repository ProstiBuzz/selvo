#ifndef VOYAGEUR_H
#define VOYAGEUR_H
#include <string>
using namespace std; 

//Creation de classe Voyageur
class Voyageur
{
    private:
    string nom;
    int age;
    string categorie;
    
    public:
    //Définition d'attributs
    //Contructeur
    Voyageur(string nom, int age);
    Voyageur();

    //Méthode d'affichage des propriétés du nom, de l'âge et de la catégorie
    void afficheDonnees();
    
    void setNom(string nom);
    string getNom();

    void setAge(int age);
    int getAge();

    void setCategorie();
    string getCategorie();

};

#endif//VOYAGEUR_H