#include <iostream>
#include <string>
#include "Voyageur.h"
using namespace std;

Voyageur::Voyageur(string nom, int age)
{
    while (nom.length() < 2)
    {
        cout << "Quel est le nom du voyageur? " << endl;
        getline(cin,nom);
    }
    this->nom=nom;
    
    while (age < 0)
    {
        cout << "Quel est l'age du voyageur? "<< endl;
        cin >> age;
    }
    this->age=age;

}
Voyageur::Voyageur(){}

//On appelle notre fonction d'affichage
void Voyageur::afficheDonnees()
{
    cout << "Voyageur: "<< this->nom <<endl;
    cout << "Age: " << this->age << endl;
};

//On vérifie la taille du nom
void Voyageur::setNom(string nom){
    while(nom.length() < 2)
    {
        cout << "Quel est le nom du voyageur? " << endl;
        getline(cin,nom);
    }
    this->nom;
}
string Voyageur::getNom(){
    return this->nom;
}


//On vérifie l'âge 
void Voyageur::setAge(int age){
    while(age <0)
    {
        cout << "Quel est l'age du voyageur? " << endl;
        cin >> age;
    }
    this->age;
}
int Voyageur::getAge(){
    return this->age;
}

int main()
{
    Voyageur voyageur1;
    voyageur1.setNom("jo");
    voyageur1.setAge(18);

    //Initialisation d'objet avec paramètres par l'utilisateur
    string nom;
    int age;
    cout << "Quel est le nom du voyageur? " << endl;
    getline(cin,nom);
    cout << "Quel est l'age du voyageur? " << endl;
    cin >> age;
    Voyageur voyageur2(nom,age);
    voyageur2.afficheDonnees();

    return 0;
}