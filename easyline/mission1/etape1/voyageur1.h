#ifndef VOYAGEUR_H
#define VOYAGEUR_H
#include <string>
using namespace std; 

//Creation de classe Voyageur
class Voyageur
{

public:
    //Définition d'attributs
    string nom;
    int age;
    //Constructeurs 
    Voyageur();
    Voyageur(string nom, int age);
    //Methode d'affichage des propriétés
    void afficherDonnees();
};

#endif//VOYAGEUR_H