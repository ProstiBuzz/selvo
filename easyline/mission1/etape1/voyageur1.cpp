#include <iostream>
#include <string>
#include "Voyageur.h"
using namespace std;

//Definition des constructeurs par defaut et à deux arguments
Voyageur::Voyageur() : nom("VoyageurX"), age(20) {}
Voyageur::Voyageur(string voyageur, int x) : nom(voyageur), age(x) {}

void Voyageur::afficherDonnees()
{
    cout << "Voyageur: " << this->nom << endl;
    cout << "Age: " << this->age << endl;
};

int main()
{
    //Initialisation du premier objet avec constructeur par défaut
    Voyageur Voyageur1;
    Voyageur1.afficherDonnees();
    //Initialisation d'objet avec paramètres par l'utilisateur
    string nom;
    int age;
    cout << "Quel est le nom du voyageur? ";
    getline(cin, nom);
    cout << "Quelle est son age? ";
    cin >> age;
    Voyageur Voyageur2(nom, age);
    Voyageur2.afficherDonnees();

    return 0;
}