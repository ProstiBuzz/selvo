#ifndef COMPAGNIE_H
#define COMPAGNIE_H
#include <string>
using namespace std;

class Compagnie
{
private:
    string nom;
    int code;
    string couleur;
    string couleur2;
    double ca
public:
    Compagnie(string nom );
    Compagnie();
    Compagnie(string nom, string c, string c2);
    Compagnie(string nom, string c, string c2, int code);

    //Methode d'affichage des propriétés
    void afficherDonees();
    
    void setNom(string nom);
    string getNom();
    
    void setCode(int code);
    int getCode();
    
    void setCouleur(string couleur);
    string getCouleur();
    
    void setCouleur2(string couleur);
    string getCouleur2();
    
    void string setCA(double c, string m);
    double getCA;
    
    friend ostream& operator<<(ostream&, const Compagnie&);
};
#endif//COMPAGNIE_H
